const express = require("express");
const axios = require("axios");
const cors = require("cors");
const app = express();

const characters = [];

app.use(cors());
app.use(express.json());

app.get("/characters", async (req, res) => {
  res.status(200).send(characters);
});

app.post("/characters", (req, res) => {
  const { name } = req.body;
  if (!name) {
    res.status(422).send({error: "name is required!"})
    return;
  }
  const id = characters.length + 1;
  const character = { id, name };

  characters.push(character);

  res.status(201).send(character);
});

app.listen(4001, () => {
  console.log("Listening on 4001");
});
