# SOA Microservice Implementation
## How to setup
```js
npm i -g nodemon //If you want to develop it further
npm i //On all individual folders
```

## How to run
### For Windows Users
```js
// This command will run a .bat file and will open several terminals
run
```
### Others
For the client run:
```js
npm start
```
For the rest run:
```js
node <folder>/index.js

// If you are developing it
nodemon <folder>/index.js
```

## Synchronous implementation
[Click here](https://gitlab.com/FadhilP/soa-demo/-/tree/sync) (setups are the same)

### Made by Fadhil Pradipta Widyanto
Credits to [Stephen Grider](https://github.com/StephenGrider) for inspiration on the app
