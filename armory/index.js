const express = require("express");
const cors = require("cors");
const app = express();

const armory = [];

app.use(cors());
app.use(express.json());

app.get("/armory", (req, res) => {
  res.send(armory);
});

app.post("/armory", (req, res) => {
  const { type, data } = req.body;
	eventHandler(type, data)
	res.status(201)
});

app.listen(4003, () => {
  console.log("Listening in 4003");
});

const eventHandler = (type, data) => {
  switch (type) {
    case "CREATE_CHARACTER":
			armory.push({...data, equipments: []})
      break;
		case "CREATE_EQUIPMENT":
			armory[armory.indexOf(armory.find(element => element.id == data.characterId))].equipments.push(data)
			break;
    default:
      break;
  }
};
