import { Fragment, useState } from "react";
import axios from "axios";

const API_URL = "http://localhost:4000/";

function App() {
  const [charName, setCharName] = useState("");
  const [equipmentName, setEquipmentName] = useState("");
  const [equipmentCharId, setEquipmentCharId] = useState("");
  const [result, setResult] = useState([]);

  const [type, setType] = useState("");
  const [error, setError] = useState("");

  const handleCharSubmit = async (event) => {
    event.preventDefault();
    try {
      await axios.post(`${API_URL}`, {
        type: "CREATE_CHARACTER",
        data: {
          name: charName,
        },
      });
      setCharName("");
      fetchCharacters();
    } catch (error) {
      setError(error.response);
    }
  };

  const handleEquipmentSubmit = async (event) => {
    event.preventDefault();
    try {
      await axios.post(`${API_URL}`, {
        type: "CREATE_EQUIPMENT",
        data: {
          name: equipmentName,
          characterId: equipmentCharId,
        },
      });
      setEquipmentName("");
      setEquipmentCharId("");
      fetchEquipments();
    } catch (error) {
      setError(error.response);
    }
  };

  const fetchCharacters = async () => {
    try {
      const response = await axios.get(`${API_URL}`, {
        params: { type: "FETCH_CHARACTER" },
      });
      setResult(response.data);
      setType("");
      setError('');
    } catch (error) {
      setError(error.response);
    }
  };

  const fetchEquipments = async () => {
    try {
      const response = await axios.get(`${API_URL}`, {
        params: { type: "FETCH_EQUIPMENT" },
      });
      setType("equipment");
      setResult(response.data);
      setError('')
    } catch (error) {
      setError(error.response);
    }
  };

  const fetchArmory = async () => {
    try {
      const response = await axios.get(`${API_URL}`, {
        params: { type: "FETCH_ARMORY" },
      });
      setType("armory");
      setResult(response.data);
    } catch (error) {
      setError(error.response);
    }
  };

  return (
    <Fragment>
      <div>
        <h1>ADD CHARACTER</h1>
        <form onSubmit={handleCharSubmit}>
          <label>Character Name</label>
          <input
            type="text"
            name="name"
            value={charName}
            onChange={(event) => setCharName(event.target.value)}
          />
          <input type="submit" value="add" />
        </form>
        <h1>ADD EQUIPMENT</h1>
        <form onSubmit={handleEquipmentSubmit}>
          <label>Equipment Name</label>
          <input
            type="text"
            name="name"
            value={equipmentName}
            onChange={(event) => setEquipmentName(event.target.value)}
          />
          <br />
          <label>Equipment Character ID</label>
          <input
            type="text"
            name="charId"
            value={equipmentCharId}
            onChange={(event) => setEquipmentCharId(event.target.value)}
          />
          <br />
          <input type="submit" value="add" />
        </form>
      </div>
      <br />
      <div>
        <button onClick={() => fetchCharacters()}>Get Characters</button>
        <button onClick={() => fetchEquipments()}>Get Equipments</button>
        <button onClick={() => fetchArmory()}>
          Get Characters with Equipments
        </button>
        <h1>{""}</h1>
        <h3>{error?.data?.error}</h3>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              {type === "equipment" && <th>CharacterID</th>}
              {type === "armory" && <th>Equipments</th>}
            </tr>
          </thead>
          <tbody>
            {result.map((value) => (
              <Fragment key={value.id}>
                <tr>
                  <td>{value?.id}</td>
                  <td>{value?.name}</td>
                  {type === "equipment" && <td>{value?.characterId}</td>}
                  <td>
                    <ul>
                      {value?.equipments?.map((eqp) => (
                        <Fragment>
                          <li key={eqp?.id}>{eqp?.name}</li>
                        </Fragment>
                      ))}
                    </ul>
                  </td>
                </tr>
              </Fragment>
            ))}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
}

export default App;
