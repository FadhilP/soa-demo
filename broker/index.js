const express = require("express");
const axios = require("axios");
const cors = require("cors");
const app = express();

app.use(cors());
app.use(express.json());

app.post("/", async (req, res) => {
  const { type, data } = req.body;
  const eventRes = await eventHandler(type, data);
	res.status(eventRes.status).send(eventRes.data);
});

app.get("/", async (req, res) => {
  const { type } = req.query;
  const eventRes = await eventHandler(type, req.query);
  res.status(eventRes.status).send(eventRes.data);
});

app.listen(4000, () => {
  console.log("Listening on 4000");
});

const eventHandler = (type, data) => {
  switch (type) {
    case "FETCH_CHARACTER":
      return axios({
        method: "GET",
        url: "http://localhost:4001/characters",
        params: data,
      })
        .then((response) => {
          return {data: response.data, status: response.status};
        })
        .catch((error) => {
          return {data: error.response.data, status: error.response.status};
        });

    case "CREATE_CHARACTER":
      return axios({
        method: "POST",
        url: "http://localhost:4001/characters",
        data,
      })
        .then((response) => {
          axios.post("http://localhost:4003/armory", {
            type,
            data: response.data,
          });
          return {data: response.data, status: response.status};
        })
        .catch((error) => {
					return {data: error.response.data, status: error.response.status};
        });

    case "FETCH_EQUIPMENT":
      return axios({
        method: "GET",
        url: "http://localhost:4002/equipments",
        params: data,
      })
        .then((response) => {
          return {data: response.data, status: response.status};
        })
        .catch((error) => {
					return {data: error.response.data, status: error.response.status};
        });
    case "CREATE_EQUIPMENT":
      return axios({
        method: "POST",
        url: "http://localhost:4002/equipments",
        data,
      })
        .then((response) => {
          axios.post("http://localhost:4003/armory", {
            type,
            data: response.data,
          });
          return {data: response.data, status: response.status};
        })
        .catch((error) => {
          return {data: error.response.data, status: error.response.status};
        });

    case "FETCH_ARMORY":
      return axios({
        method: "GET",
        url: "http://localhost:4003/armory",
        data,
      })
        .then((response) => {
          return {data: response.data, status: response.status};
        })
        .catch((error) => {
          return {data: error.response.data, status: error.response.status};
        });

    default:
      return;
  }
};
