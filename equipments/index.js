const express = require("express");
const cors = require("cors");
const app = express();

const equipments = [];

app.use(cors());
app.use(express.json());

app.get("/equipments", (req, res) => {
  res.status(200).send(equipments);
});

app.post("/equipments", (req, res) => {
  const { name, characterId } = req.body;
  if (!characterId && !name){
    res.status(422).send({ error: 'Name and characterId are required!' })
    return;
  }
  const id = equipments.length + 1;
  const equipment = { id, name, characterId };

  equipments.push(equipment);

  res.status(201).send(equipment);
});

app.listen(4002, () => {
  console.log("Listening on 4002");
});
